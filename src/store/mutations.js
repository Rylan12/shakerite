export const SET_FONT_SIZE = 'setFontSize';
export const SET_FONT_WEIGHT = 'setFontWeight';
export const SET_FONT_NAME = 'setFontName';
export const SET_COLOR_BG = 'setBGColor';
export const SET_COLOR_FG = 'setFGColor';
export const ADD_ARTICLE = 'addArticle';
export const SET_ARTICLES = 'setArticle';
export const SET_CATEGORY = 'setCategory';
export const ADD_CACHED = 'addCache';
export const ADD_CACHED_IMAGE = 'addCacheImage';
export const SET_SAVED_ARTICLES = 'setSavedArticles';
export const SET_BROWSER = 'setBrowser';
export default {
	SET_FONT_SIZE,
	SET_FONT_WEIGHT,
	SET_FONT_NAME,
	SET_COLOR_BG,
	SET_COLOR_FG,
	ADD_ARTICLE,
	SET_ARTICLES,
	SET_CATEGORY,
	ADD_CACHED,
	ADD_CACHED_IMAGE,
	SET_SAVED_ARTICLES,
	SET_BROWSER,
};
